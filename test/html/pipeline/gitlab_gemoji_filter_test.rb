require 'test_helper'
require 'html/pipeline/gitlab'

class HTML::Pipeline::GitlabEmojiFilterTest < Minitest::Test
  GitlabEmojiFilter = HTML::Pipeline::Gitlab::GitlabEmojiFilter

  def test_emojify
    filter = GitlabEmojiFilter.new('<p>:heart:</p>', { asset_root: 'https://foo.com' })
    doc = filter.call
    assert_match 'https://foo.com/emoji/heart.png', doc.search('img').attr('src').value
  end

  def test_unsupported_emoji
    block = '<p>:sheep:</p>'
    filter = GitlabEmojiFilter.new(block, { asset_root: 'https://foo.com' })
    doc = filter.call
    assert_match block, doc.to_html
  end

  def test_uri_encoding
    filter = GitlabEmojiFilter.new('<p>:+1:</p>', { asset_root: 'https://foo.com' })
    doc = filter.call
    assert_match 'https://foo.com/emoji/%2B1.png', doc.search('img').attr('src').value
  end

  def test_required_context_validation
    exception = assert_raises(ArgumentError) {
      GitlabEmojiFilter.call("", {})
    }
    assert_match /:asset_root/, exception.message
  end
end
