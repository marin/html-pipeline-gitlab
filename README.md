# Html::Pipeline::Gitlab

This gem implements various filters for [html-pipeline](https://github.com/jch/html-pipeline)
 used by [GitLab](https://about.gitlab.com).

[![Build Status](https://semaphoreapp.com/api/v1/projects/b9b808be-6c72-4e76-ae62-79b3a25a022a/243365/badge.png)](https://semaphoreapp.com/razer6/html-pipeline-gitlab)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'html-pipeline-gitlab'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install html-pipeline-gitlab

## Testing

Install required gems

```bash
bundle
```

and then run the test suite:

```bash
bundle exec rake test
```

## Filters

* `GitlabEmojiFilter` - replaces emoji references with images from
[PhantomOpenEmoji](https://github.com/Genshin/PhantomOpenEmoji)

## Contributing

1. Fork it
1. Create your feature branch (`git checkout -b my-new-feature`)
1. Commit your changes (`git commit -am 'Add some feature'`)
1. Push to the branch (`git push origin my-new-feature`)
1. Create a new Merge Request
